﻿import json

class Sql_script_generation:
    
    def create_dim_tables_script(self):

        with open("B_dimension_config.json","r") as json_file: 
            dimension_file = json.load(json_file)
            with open("create_dimension_tables.sql", 'w+') as file:
                for val in dimension_file['dimension_data']:
                    # print('dimension: ' + val['dimension'])
                    d_table_name=(val['dimension']).split('-',1)[0]
                    d_column_name=(val['dimension']).split('-',1)[1]
                    create_table_query="\n -- ============="+d_column_name+"=============\n create table if not exists "+d_table_name+" (`id` INT (11) AUTO_INCREMENT,"+d_column_name+" VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL UNIQUE,`inserted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`),UNIQUE INDEX `Index 2` ("+d_column_name+")); \n"
                    # print(create_table_query)        
                    file.write(create_table_query)
            file.close()
        message="Generated <<create_dimension_tables.sql>> file for dimensions successfully"
        return (message)
    
    def create_raw_temp_table(self):
        m_query=" "
        d_query=" "
        
        with open("B_metrics_config.json","r") as json_file: 
            metric_file = json.load(json_file)
            for val in metric_file['metric_data']:
                # print('metric: ' + val['metric'])
                table_name=(val['metric']).split('-',1)[0]
                column_name=(val['metric']).split('-',1)[1]
                if val['metric-unit']=='KMB':
                    column_name= val['gid_column'] +" INT (11) NOT NULL DEFAULT (0) , \n" 
                elif val['metric-unit']=='$':
                    column_name= val['gid_column']+" DOUBLE NOT NULL DEFAULT (0) , \n" 
                # print (column_name)
                # print("")

                m_query +=column_name
                
        # print (m_query)

        with open("B_dimension_config.json","r") as json_file: 
            dimension_file = json.load(json_file)
            for val in dimension_file['dimension_data']:
                # print('target-column: ' + val['target-column'])
                column_name=val['target-column']+ " INT(9) NOT NULL DEFAULT '0' , \n"
                d_query += column_name 
                
            # print("\n dimension query part:-\n",d_query)    

        raw_temp_table_query=" CREATE TABLE IF NOT EXISTS "+table_name+"_raw_temp \n(`time_key` INT(11) NOT NULL DEFAULT '0' \n,"+d_query+""+m_query+" `inserted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,\n `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ""); \n"
        
        # with open(table_name+"_raw_temp.sql", 'w+') as file:
        #     file.write(raw_temp_table_query)
        # file.close()
        # return (table_name)
        
        # print("Generated <<"+table_name+"_raw_temp.sql>> for temp table successfully\n")
        return(raw_temp_table_query)


    def create_raw_table(self,raw_temp_table_query):
        with open("B_metrics_config.json","r") as json_file: 
            metric_file = json.load(json_file)
            for val in metric_file['metric_data']:
                raw_table_name=(val['metric']).split('-',1)[0]


        create_raw_table_sql="CREATE TABLE IF NOT EXISTS "+raw_table_name+"_raw as (select * from "+raw_table_name+"_raw_temp where 1=2); \n"

        # print (create_raw_table_sql)

        with open("B_dimension_config.json","r") as json_file: 
            dimension_file = json.load(json_file)
            with open("create_temp_and_raw_table.sql", 'w+') as file:
                file.write("-- =================temp_raw table creation query =================\n\n")
                file.write(raw_temp_table_query)
                file.write("-- =================temp_raw table creation query  ended ================= \n\n")
                

                file.write("-- =================raw table creation query =================\n\n")
                file.write(create_raw_table_sql)
                file.write("-- =================raw table creation query  ended ================= \n\n")
                file.write("-- ============== Applying ALTER COMMAND ================= \n\n")
                for val in dimension_file['dimension_data']:
                    d_table_name=(val['dimension']).split('-',1)[0]
                    foreign_key_name=(val['target-column'])
                    # print(d_table_name)
                    # print(foreign_key_name)
                    file.write("-- applying "+foreign_key_name+ "  as foreign key \n")
                    alter_command="ALTER TABLE "+raw_table_name+"_raw ADD CONSTRAINT   `FK_"+raw_table_name+"_"+d_table_name+"`  FOREIGN KEY  ("+foreign_key_name+ ")  REFERENCES `"+d_table_name+"` (`id`) ON UPDATE CASCADE ON DELETE CASCADE; \n\n\n"
                    # print(alter_command)
                    
                    file.write(alter_command)
                file.close()
        print("Generated <<create_temp_and_raw_table.sql>> file for the '"+raw_table_name+"_raw_temp' and '"+raw_table_name+"_raw' table \n")
        





print('')
object_A=Sql_script_generation()
print(object_A.create_dim_tables_script())

print('')
raw_temp_table_query=(object_A.create_raw_temp_table())

object_A.create_raw_table(raw_temp_table_query)